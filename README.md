# README #

## Info ##
- Zuul plugin for Chrome, by Tom du Plessis
- Version:  0.1 Beta
- Please send comments/suggestions to ThomasD2@discovery.co.za
- https://bitbucket.org/tomdupld/zuul-plugin
- Feel free to share, distribte and re-use

## Installation ##
1.  Unzip/clone this folder to your hard disk by running, in the command prompt:
       `git clone https://tomdupld@bitbucket.org/tomdupld/zuul-plugin.git`
2.  In Chrome, go to Menu -> More -> Extensions
3.  Check the 'Developer Mode' checkbox
4.  Click "Load unpacked extension" and select the directory that you just extracted in step 1
5.  Reload your Zuul directory page and enjoy
