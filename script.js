var items = [];

$( document ).ready(function() {


    // Fetch the raw JSON from the server and process.  This is to gracefully handle any JSON viewer plugins that
    // a user may have installed.
    $.getJSON( window.location.href, function( data ) {
      $.each( data, function( key, val ) {
        items.push(sanitizeURL(key));
      });
      items.sort();
      renderHtml();

    });


});

// Render the actual HTML and replace page contents with it
function renderHtml() {
    var body = $('body');
    var host = (window.location.protocol + "//" + window.location.host).replace('/manage/', '/');;

    var html = "<table class='TFtable' id='tbl'>";
    for (i=0;i<items.length;i++) {
        html += renderItem(host, items[i]);
    }
    html +="</table>";
    body.html(html);
}

function renderItem(host, val) {
    var row = "<tr>"
    + "<td>" + capitalize(val) + "</td>"
    + "<td><a class='swagger' href='"+host+"/"+val+"'>Swagger</a></td>"
    + "<td><a class='info' href='"+host+"/"+val+"manage/info'>Info</a></td>"
    + "<td><a class='env' href='"+host+"/config/"+val+"test-env'>Env</a></td>"
    + "<td><span class='actual'>&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;/" + val + "</span></td>"
    + "</tr>";
    return row;
}

// Removes the '-' and capitalises the first character of every word
function capitalize(txt) {
    var ret = "";
    var words = txt.split(/[- \/]/);
    for (j=0;j<words.length;j++) {
        ret += " " + words[j].charAt(0).toUpperCase() + words[j].slice(1);
    }
    return ret;
}

function sanitizeURL(txt) {
    return txt.replace('**', '').replace('/', '');
}

